﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Autofac;
using Autofac.Builder;
using Autofac.Integration.Mvc;
using RoomEscapeCatalog.Models;

namespace RoomEscapeCatalog.Services
{
    public static class AutofacConfig
    {
        public static void SetDependencyResolver()
        {
            var builder = new Autofac.ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            //builder.RegisterType<RoomsRepository>().As<IRepository<Room>>().SingleInstance();
            builder.RegisterType<RoomsRepositoryEf>().As<IRepository<Room>>().WithParameter("connectionString", "RoomsCatalogEf");

            var container = builder.Build();

            // create repositories
            //var repositoryGenerator = new RepositoryGenerator(container);
            //repositoryGenerator.PopulateRoomRepository();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}