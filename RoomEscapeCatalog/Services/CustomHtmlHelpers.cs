﻿using Microsoft.Ajax.Utilities;
using RoomEscapeCatalog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace RoomEscapeCatalog.Services
{
    public static class CustomHtmlHelpers
    {
        public static MvcHtmlString RoomCatalogItem(this HtmlHelper html, Room room)
        {
            var divContainer = new TagBuilder("div");
            divContainer.AddCssClass("room");

            divContainer.InnerHtml += html.ActionLink(
                room.Name,
                nameof(Controllers.HomeController.RoomDetails),
                routeValues: new { roomName = room.NameID },
                htmlAttributes: new { @class = "name" });

            var description = new TagBuilder("div");
            description.AddCssClass("description");
            description.SetInnerText(room.ShortDescription);
            divContainer.InnerHtml += description;

            var details = new TagBuilder("div");
            details.AddCssClass("details");
            
            // difficulty
            var difficultyLbl = new TagBuilder("div");
            difficultyLbl.SetInnerText("Difficulty");
            details.InnerHtml += difficultyLbl;

            var difficulty = new TagBuilder("div");
            difficulty.SetInnerText($"{room.Difficulty}");
            details.InnerHtml += difficulty;

            // group size
            var groupSizeLbl = new TagBuilder("div");
            groupSizeLbl.SetInnerText("Group size");
            details.InnerHtml += groupSizeLbl;

            var groupSize = new TagBuilder("div");
            groupSize.SetInnerText($"{room.MinPlayers}-{room.MaxPlayers}");
            details.InnerHtml += groupSize;

            // time limit
            var timeLimitLbl = new TagBuilder("div");
            timeLimitLbl.SetInnerText("Time limit");
            details.InnerHtml += timeLimitLbl;

            var timeLimit = new TagBuilder("div");
            timeLimit.SetInnerText($"{room.DurationMinutes} min");
            details.InnerHtml += timeLimit;

            divContainer.InnerHtml += details;

            var imageSpan = new TagBuilder("span");
            imageSpan.AddCssClass("image");
            var imagePath = VirtualPathUtility.ToAbsolute($"~/{room.Logo.Path}");
            imageSpan.MergeAttribute("style", $"background-image: url(\"{ imagePath }\")");
            divContainer.InnerHtml += imageSpan;

            divContainer.InnerHtml += html.ActionLink(
                "MORE INFO >",
                nameof(Controllers.HomeController.RoomDetails),
                routeValues: new { roomName = room.NameID },
                htmlAttributes: new { @class = "btn btn-outline-dark" });

            return MvcHtmlString.Create(divContainer.ToString());
        }
    }
}