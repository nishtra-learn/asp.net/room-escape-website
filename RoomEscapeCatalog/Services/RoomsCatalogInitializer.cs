﻿using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using RoomEscapeCatalog.Models;

namespace RoomEscapeCatalog.Services
{
    public class RoomsCatalogInitializer : CreateDatabaseIfNotExists<RoomsCatalogContext>
    {
        protected override void Seed(RoomsCatalogContext context)
        {
            Room room = null;
            // DaVinci's Workshop
            room = new Room()
            {
                Name = "DaVinci’s Workshop",
                ShortDescription = "Can you save the Mona Lisa from burglars?",
                Overview = "Thieves are on their way to steal the Mona Lisa right now! Can you get to the painting first and" +
                "save DaVinci’s greatest work of art in this St. Charles escape room?\n" +
                "The year is 1519.Leonardo DaVinci has just received word via carrier pigeon that thieves are en route" +
                "to ransack his workshop and steal the Mona Lisa in this St.Charles escape room.On hearing the shocking news," +
                "the artist is pleading for somebody…anybody…to save his famous painting.Working with your team of 2-8 heroes," +
                "you have 60 exhilarating minutes to get the masterpiece to safety before the thieves arrive.\n" +
                "Complete this beginner-level escape room to save the painting, and DaVinci will reward you handsomely." +
                "Arrive after the thieves and the Mona Lisa will be lost forever.",
                DurationMinutes = 60,
                MinPlayers = 2,
                MaxPlayers = 8,
                AgeRestriction = 12,
                Address = "St Louis",
                Phone = "500-444-25-52",
                Email = "roomescape@mail.com",
                Rating = 4,
                Creepiness = 1,
                Difficulty = 2
            };
            room.Logo = new ImageData
            {
                Filename = "davinci_logo.jpg",
                Path = Path.Combine("Files", "davinci-logo.jpg")
            };
            room.Gallery = new List<ImageData>() {
                new ImageData {
                    Filename = "davinci_logo.jpg",
                    Path = Path.Combine("Files", "davinci-logo.jpg")
                },
                new ImageData {
                    Filename = "aerial-screw.jpg",
                    Path = Path.Combine("Files", "aerial-screw.jpg")
                },
            };
            context.Rooms.Add(room);


            // Cursed Castle
            room = new Room()
            {
                Name = "Cursed Castle",
                ShortDescription = "Can you save the kingdom from eternal darkness?",
                Overview = "You all are loyal knights of a benevolent kingdom returning from a faraway journey." +
                "As you approach your town, you see dark, ominous clouds. You arrive to find your castle eerily dark, " +
                "quiet and sealed shut. Local villagers have told you that no one has been seen going in or out for days. " +
                "Recognizing the classic signs of an evil curse, you decide it is your duty to do whatever you can to lift it, " +
                "but you won’t be able to survive inside the curse for more than an hour so you must move quickly. " +
                "If you can lift the curse, you will save your kingdom, BUT if your kingdom will be plunged into darkness and " +
                "you will be sealed inside the castle forever. You have 60 minutes – Good luck!",
                DurationMinutes = 60,
                MinPlayers = 2,
                MaxPlayers = 12,
                AgeRestriction = 14,
                Address = "St Louis",
                Phone = "500-444-25-52",
                Email = "roomescape@mail.com",
                Rating = 5,
                Creepiness = 3,
                Difficulty = 5
            };
            room.Logo = new ImageData
            {
                Filename = "castle-escape-room.jpg",
                Path = Path.Combine("Files", "castle-escape-room.jpg")
            };
            room.Gallery = new List<ImageData>() {
                new ImageData {
                    Filename = "castle-dining-room.jpg",
                    Path = Path.Combine("Files", "castle-dining-room.jpg")
                },
                new ImageData {
                    Filename = "castle-interior-1.jpg",
                    Path = Path.Combine("Files", "castle-interior-1.jpg")
                },
                new ImageData {
                    Filename = "castle-interior-2.jpg",
                    Path = Path.Combine("Files", "castle-interior-2.jpg")
                },
            };
            context.Rooms.Add(room);


            // Diamond Heist
            room = new Room()
            {
                Name = "Diamond Heist",
                ShortDescription = "Think you’ve got what it takes to be the world’s greatest thief?",
                Overview = "The largest diamond in the world is almost in your reach. Do you have what it takes to grab " +
                "the treasure and get out before the police arrive in this heist escape room?\n" +
                "Being a person of(devious) action, you’ve pulled together a team of the best thieves around.You have a plan, " +
                "and it’s time to pull off the biggest diamond heist in history in this heist escape room! " +
                "Working with your team of 2 - 10 crooks, you have 60 heart - pumping minutes to get the diamond before the police arrive.\n" +
                "Execute the plan flawlessly, and you’ll be rich beyond your wildest dreams. " +
                "Fail, and you’ll all be spending the rest of your life in jail.",
                DurationMinutes = 60,
                MinPlayers = 2,
                MaxPlayers = 10,
                AgeRestriction = 12,
                Address = "St Louis",
                Phone = "500-444-25-52",
                Email = "roomescape@mail.com",
                Rating = 4,
                Creepiness = 1,
                Difficulty = 3
            };
            room.Logo = new ImageData
            {
                Filename = "diamond-big.png",
                Path = Path.Combine("Files", "diamond-big.png")
            };
            room.Gallery = new List<ImageData>() {
                new ImageData {
                    Filename = "diamond-big.png",
                    Path = Path.Combine("Files", "diamond-big.png")
                },
                new ImageData {
                    Filename = "diamond-museum.jpg",
                    Path = Path.Combine("Files", "diamond-museum.jpg")
                },
            };
            context.Rooms.Add(room);


            // Secret Society
            room = new Room()
            {
                Name = "Secret Society",
                ShortDescription = "Will you survive the trials of a Secret Society’s initiation?",
                Overview = "You poked your nose where it doesn’t belong and got more than you bargained for. " +
                "Now, will you be initiated, or eliminated in this suspenseful escape room?\n" +
                "You and your friends have been researching an ancient secret society, only to make the startling " +
                "discovery that they’ve been researching YOU! Soon after making your discovery, you find yourself kidnapped and " +
                "locked in a room deep within the walls of a medieval castle in this suspenseful escape room. " +
                "Your only chance for escape is to complete the Initiation Trial successfully.Work together with your group of 2 - 8 " +
                "initiates to find the clues and solve the puzzles.You’ve got 60 minutes to escape the room.\n" +
                "This challenging escape room is full of twists and turns to keep your adrenaline pumping the entire time. " +
                "Escape and everyone gains membership in the secret society.Can’t complete the Initiation Trial? Face elimination.",
                DurationMinutes = 60,
                MinPlayers = 2,
                MaxPlayers = 8,
                AgeRestriction = 14,
                Address = "St Louis",
                Phone = "500-444-25-52",
                Email = "roomescape@mail.com",
                Rating = 5,
                Creepiness = 3,
                Difficulty = 5
            };
            room.Logo = new ImageData
            {
                Filename = "secret-society-desk.jpg",
                Path = Path.Combine("Files", "secret-society-desk.jpg")
            };
            room.Gallery = new List<ImageData>() {
                new ImageData {
                    Filename = "secret-society-desk.jpg",
                    Path = Path.Combine("Files", "secret-society-desk.jpg")
                },
                new ImageData {
                    Filename = "secret-society-desk-candle.jpg",
                    Path = Path.Combine("Files", "secret-society-desk-candle.jpg")
                },
                new ImageData {
                    Filename = "secret-society-desk-book-candle.jpg",
                    Path = Path.Combine("Files", "secret-society-desk-book-candle.jpg")
                },
            };
            context.Rooms.Add(room);
            
            context.SaveChanges();
            base.Seed(context);
        }
    }
}