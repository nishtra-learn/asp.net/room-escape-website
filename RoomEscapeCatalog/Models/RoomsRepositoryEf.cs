﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace RoomEscapeCatalog.Models
{
    public class RoomsRepositoryEf : IRepository<Room>, IDisposable
    {
        private RoomsCatalogContext db;

        public RoomsRepositoryEf(string connectionString)
        {
            db = new RoomsCatalogContext(connectionString);
        }

        public int Length => db.Rooms.Count();

        public Room Add(Room obj)
        {
            db.Entry(obj).State = EntityState.Added;

            foreach (var img in obj.Gallery)
            {
                db.Entry(img).State = EntityState.Added;
            }

            db.SaveChanges();
            return obj;
        }

        public bool Delete(int id)
        {
            var roomToDelete = db.Rooms.Find(id);

            if (roomToDelete != null)
            {
                // delete related image entries
                var relatedImages = roomToDelete.Gallery.ToList();
                relatedImages.Add(roomToDelete.Logo);
                foreach (var img in relatedImages)
                {
                    db.Images.Remove(img);
                }

                // remove room
                db.Rooms.Remove(roomToDelete);
                db.SaveChanges();
            }

            return roomToDelete != null;
        }

        public IEnumerable<Room> GetAll()
        {
            return db.Rooms.AsEnumerable();
        }

        public Room GetById(int id)
        {
            return db.Rooms.Find(id);
        }

        public Room Update(Room obj)
        {
            // update room properties
            var existingRoom = db.Rooms.Find(obj.Id);
            db.Entry(existingRoom).CurrentValues.SetValues(obj);

            var outdatedImages = existingRoom.Gallery
                .Where(im => {
                    bool isNotInUpdatedGallery = obj.Gallery.FirstOrDefault(newIm => im.Id == newIm.Id) == null;
                    return isNotInUpdatedGallery;
                })
                .ToList();
            if (existingRoom.Logo.Id != obj.Logo.Id)
                outdatedImages.Add(existingRoom.Logo);

            // remove outdated images
            foreach (var img in outdatedImages)
            {
                db.Entry(img).State = EntityState.Deleted;
            }

            // update logo
            if (obj.Logo.Id == -1)
                db.Entry(obj.Logo).State = EntityState.Added;

            // add/update images in the modified Room entry
            foreach (var img in obj.Gallery)
            {
                if (img.Id > 0)
                {
                    var trackedImage = db.Set<ImageData>().Local.FirstOrDefault(t => t.Id == img.Id);
                    if (trackedImage != null)
                        db.Entry(trackedImage).State = EntityState.Detached;
                    db.Entry(img).State = EntityState.Modified;
                }
                else
                    db.Entry(img).State = EntityState.Added;
            }

            db.SaveChanges();
            return obj;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    db.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~RoomsRepositoryEf()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}