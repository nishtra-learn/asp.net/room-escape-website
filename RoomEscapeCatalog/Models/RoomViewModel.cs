﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoomEscapeCatalog.Models
{
    public class RoomViewModel
    {
        public Room Room { get; set; }
        public HttpPostedFileBase LogoImageData { get; set; }
        public List<HttpPostedFileBase> GalleryImageData { get; set; }

        public RoomViewModel()
        {
            Room = new Room();
        }

        public RoomViewModel(Room room)
        {
            Room = room;
        }
    }
}