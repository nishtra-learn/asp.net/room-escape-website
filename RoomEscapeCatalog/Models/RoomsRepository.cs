﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoomEscapeCatalog.Models
{
    public class RoomsRepository : IRepository<Room>
    {
        private List<Room> rooms = new List<Room>();

        public int Length => rooms.Count;

        public Room Add(Room obj)
        {
            obj.Id = rooms.Count > 0
                ? rooms.Max(room => room.Id) + 1
                : 1;
            rooms.Add(obj);
            return obj;
        }

        public bool Delete(int id)
        {
            int count = rooms.RemoveAll(room => room.Id == id);
            return count > 0 ? true : false;
        }

        public IEnumerable<Room> GetAll()
        {
            return rooms.AsEnumerable();
        }

        public Room GetById(int id)
        {
            return rooms.FirstOrDefault(room => room.Id == id);
        }

        public Room Update(Room obj)
        {
            var roomToUpdate = rooms.FirstOrDefault(room => room.Id == obj.Id);
            
            if (roomToUpdate == null)
                return roomToUpdate;
            
            roomToUpdate.Name = obj.Name;
            roomToUpdate.Overview = obj.Overview;
            roomToUpdate.DurationMinutes = obj.DurationMinutes;
            roomToUpdate.MinPlayers = obj.MinPlayers;
            roomToUpdate.MaxPlayers = obj.MaxPlayers;
            roomToUpdate.AgeRestriction = obj.AgeRestriction;
            roomToUpdate.Address = obj.Address;
            roomToUpdate.Phone = obj.Phone;
            roomToUpdate.Email = obj.Email;
            roomToUpdate.Rating = obj.Rating;
            roomToUpdate.Creepiness = obj.Creepiness;
            roomToUpdate.Difficulty = obj.Difficulty;
            roomToUpdate.Logo = obj.Logo;
            roomToUpdate.Gallery = obj.Gallery;

            return roomToUpdate;
        }
    }
}