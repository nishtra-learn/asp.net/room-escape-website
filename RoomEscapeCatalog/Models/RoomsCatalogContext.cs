﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RoomEscapeCatalog.Models
{
    public class RoomsCatalogContext : DbContext
    {
        public DbSet<Room> Rooms { get; set; }
        public DbSet<ImageData> Images { get; set; }

        public RoomsCatalogContext(string connectionString) : base(connectionString) 
        {
            Database.SetInitializer<RoomsCatalogContext>(new Services.RoomsCatalogInitializer());
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ImageData>()
                .HasOptional(i => i.LogoOwnerRoom)
                .WithMany(r => r.LogoCollection)
                .HasForeignKey(i => i.LogoOwnerRoomId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ImageData>()
                .HasOptional(i => i.GalleryOwnerRoom)
                .WithMany(r => r.Gallery)
                .HasForeignKey(i => i.GalleryOwnerRoomId)
                .WillCascadeOnDelete(false);

            base.OnModelCreating(modelBuilder);
        }
    }
}