﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomEscapeCatalog.Models
{
    public interface IRepository<T>
    {
        IEnumerable<T> GetAll();
        int Length { get; }

        T GetById(int id);
        T Add(T obj);
        T Update(T obj);
        bool Delete(int id);
    }
}
