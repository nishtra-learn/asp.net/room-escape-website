﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace RoomEscapeCatalog.Models
{
    public class Room
    {
        [Key]
        public int Id { get; set; } = -1;

        private string nameID;
        [NotMapped]
        public string NameID
        {
            get
            {
                if (nameID != null)
                    return nameID;

                var normalizedName = Name.ToLower();
                normalizedName = Regex.Replace(normalizedName, @"[\s-]+", "_");
                normalizedName = Regex.Replace(normalizedName, @"[\W]+", "");
                nameID = normalizedName;
                return nameID;
            }
        }

        public string Name { get; set; }
        public string ShortDescription { get; set; } = "";
        public string Overview { get; set; } = "";
        public double DurationMinutes { get; set; } = 60;
        public int MinPlayers { get; set; } = 1;
        public int MaxPlayers { get; set; } = 1;
        public int AgeRestriction { get; set; } = 12;
        public string Address { get; set; } = "";
        public string Phone { get; set; } = "";
        public string Email { get; set; } = "";
        public int? Rating { get; set; }
        public int Creepiness { get; set; } = 1;
        public int Difficulty { get; set; } = 1;

        [NotMapped]
        public ImageData Logo
        {
            get
            {
                return LogoCollection.SingleOrDefault();
            }
            set
            {
                LogoCollection.Clear();
                LogoCollection.Add(value);
            }
        }

        public virtual ICollection<ImageData> LogoCollection { get; set; }
        public virtual ICollection<ImageData> Gallery { get; set; }

        public Room()
        {
            Gallery = new List<ImageData>();
            LogoCollection = new List<ImageData>();
        }
    }
}