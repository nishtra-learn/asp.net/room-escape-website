﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RoomEscapeCatalog.Models
{
    [Table("Images")]
    public class ImageData
    {
        [Key]
        public int Id { get; set; } = -1;
        public string Filename { get; set; } = "";
        public string Path { get; set; } = "";
        public byte[] RawData { get; set; }

        public int? LogoOwnerRoomId { get; set; }
        public virtual Room LogoOwnerRoom { get; set; }

        public int? GalleryOwnerRoomId { get; set; }
        public virtual Room GalleryOwnerRoom { get; set; }
    }
}