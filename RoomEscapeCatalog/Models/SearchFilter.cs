﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoomEscapeCatalog.Models
{
    public class SearchFilter
    {
        public int DifficultyMin { get; set; }
        public int DifficultyMax { get; set; }
        public int CreepinessMin { get; set; }
        public int CreepinessMax { get; set; }
        public int GroupSizeMin { get; set; }
        public int GroupSizeMax { get; set; }
    }
}