﻿using RoomEscapeCatalog.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RoomEscapeCatalog.Controllers
{
    //[RoutePrefix("admin")]
    [Route("admin/{action=Index}/{id?}")]
    public class AdminController : Controller
    {
        private IRepository<Room> rooms;

        public AdminController(IRepository<Room> rooms)
        {
            this.rooms = rooms;
        }

        // GET: Admin
        public ActionResult Index()
        {
            return View(rooms.GetAll());
        }


        // CREATE
        [Route("rooms/add")]
        [HttpGet]
        public ActionResult AddRoom()
        {
            return View(new RoomViewModel());
        }

        [Route("rooms/add")]
        [HttpPost]
        public ActionResult AddRoom(RoomViewModel roomVm)
        {
            ProcessSubmittedRoomVm(roomVm);
            rooms.Add(roomVm.Room);
            return RedirectToAction(nameof(Index));
        }

        // EDIT
        [Route("rooms/edit/{id:int}")]
        [HttpGet]
        public ActionResult EditRoom(int id)
        {
            var roomToEdit = rooms.GetAll().FirstOrDefault(room => room.Id == id);

            if (roomToEdit == null)
                return HttpNotFound($"A room with ID {id} couldn't be found");

            var roomVm = new RoomViewModel(roomToEdit);
            return View(roomVm);
        }

        [Route("rooms/edit/{id:int}")]
        [HttpPost]
        public ActionResult EditRoom(RoomViewModel roomVm)
        {
            ProcessSubmittedRoomVm(roomVm);
            rooms.Update(roomVm.Room);
            return RedirectToAction(nameof(Index));
        }

        // DELETE
        [Route("rooms/delete/{id:int}")]
        [HttpGet]
        public ActionResult DeleteRoom(int id)
        {
            rooms.Delete(id);
            return RedirectToAction(nameof(Index));
        }


        #region Private methods
        private void ProcessSubmittedRoomVm(RoomViewModel roomVm)
        {
            if (roomVm.LogoImageData != null)
            {
                var relPath = SavePostedFile(roomVm.LogoImageData);
                if (roomVm.Room.Logo == null)
                    roomVm.Room.Logo = new ImageData();
                roomVm.Room.Logo.Filename = roomVm.LogoImageData.FileName;
                roomVm.Room.Logo.Path = relPath;
                roomVm.Room.Logo.LogoOwnerRoomId = roomVm.Room.Id;
            }

            if (roomVm.GalleryImageData?.Count > 0)
            {
                foreach (var file in roomVm.GalleryImageData)
                {
                    if (file == null)
                        continue;

                    var imageData = new ImageData();
                    var relPath = SavePostedFile(file);
                    imageData.Filename = file.FileName;
                    imageData.Path = relPath;
                    imageData.GalleryOwnerRoomId = roomVm.Room.Id;
                    roomVm.Room.Gallery.Add(imageData);
                }
            }
        }

        private string SavePostedFile(HttpPostedFileBase uploadedFile)
        {
            var filename = uploadedFile.FileName;
            var relativePath = Path.Combine("Files", filename);
            var absolutePath = Server.MapPath($"~/{relativePath}");
            uploadedFile.SaveAs(absolutePath);

            return relativePath;
        }
        #endregion
    }
}