﻿using RoomEscapeCatalog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RoomEscapeCatalog.Controllers
{
    public class HomeController : Controller
    {
        private IRepository<Room> rooms;


        public HomeController(IRepository<Room> rooms)
        {
            this.rooms = rooms;
        }


        public ActionResult Index()
        {
            return View(rooms.GetAll());
        }


        [Route("room/{roomName}")]
        public ActionResult RoomDetails(string roomName)
        {
            var normalizedRoomName = roomName.ToLower();
            var room = rooms
                .GetAll()
                .FirstOrDefault(r => r.NameID == normalizedRoomName);

            if (room == null)
            {
                return HttpNotFound();
            }

            return View(room);
        }


        public ActionResult Search(SearchFilter filter)
        {
            var unfiltered = rooms.GetAll().ToList();
            var filtered = unfiltered.Where(t => 
                t.Creepiness >= filter.CreepinessMin &&
                t.Creepiness <= filter.CreepinessMax &&
                t.Difficulty >= filter.DifficultyMin &&
                t.Difficulty <= filter.DifficultyMax &&
                t.MinPlayers >= filter.GroupSizeMin &&
                t.MaxPlayers <= filter.GroupSizeMax)
                .ToList();

            /* pass the whole room collection for carousel*/
            ViewBag.RoomsAll = unfiltered;

            return View("Index", filtered);
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }


        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}